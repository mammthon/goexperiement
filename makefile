output=main
namespace=goexperiment
Version=`git describe --tag  --abbrev=0`
build_time=`date +%FT%T%z`
commit_hash=`git rev-parse --short HEAD`

.PHONY: swagger-bot
swagger-bot: swag-app
	@sh swagger-bot.sh

format:
	$(shell go env GOPATH)/bin/gofumpt -w app
	gofumpt -w -l app
	$(shell go env GOPATH)/bin/gci write -s standard -s default app
	$(shell go env GOPATH)/bin/goimports -w app

.PHONY: run-app
run-app:  build-app
	@./build/goexperimentApp

.PHONY: build-app
build-app:
	@echo $(env 2, $(filter-out $@,$(MAKECMDGOALS)))
	go build -tags dynamic -ldflags "-X main.NameSpace=App \
			-X main.CommitHash=${commit_hash} \
			-X main.Env=${env} \
			-X main.BuildTime=${build_time} " \
			-o ./build/goexperimentApp \
			./app/api/src/cmd

swag-api:
	swag init -d app/api --parseDependency --parseInternal -g ./src/cmd/main.go
	docker-compose -f swagger-docker-compose.yml up -d --remove-orphans

GoLinter:
	@go list -e -compiled -test=true -export=false -deps=true -find=false -tags=dynamic -- ./... > /dev/null
	@golangci-lint run app/admin/... app/common/... app/api/... app/pkg/...

GoLinter-setup:
	@brew install golangci-lint
	@golangci-lint --version

pre-hook-setup:
	@brew install pre-commit
	@pip3 install pre-commit
	@pre-commit install
	@pre-commit install --hook-type commit-msg

setup:
	go install -v golang.org/x/tools/cmd/goimports@latest
	go install -v github.com/daixiang0/gci@latest
	go install mvdan.cc/gofumpt@latest
	go install github.com/fzipp/gocyclo/cmd/gocyclo@latest
	go install golang.org/x/tools/cmd/goimports@latest  # required by `go-imports`
	go install github.com/fzipp/gocyclo/cmd/gocyclo@latest  # required by `go-cyclo`
	go install github.com/go-critic/go-critic/cmd/gocritic@latest  # required by `go-critic`
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest  # required by `golangci-lint`
	go install github.com/swaggo/swag/cmd/swag@latest
	go install golang.org/x/vuln/cmd/govulncheck@latest
	go install github.com/securego/gosec/cmd/gosec@latest


tidy-api:
	cd ${GOPATH}/src/dentistease && cd app/api && go mod tidy

tidy-pkg:
	cd ${GOPATH}/src/dentistease && cd app/pkg && go mod tidy

