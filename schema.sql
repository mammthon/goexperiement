create database gen;
create table if not exists gen.credit_cards
(
    id      bigint unsigned auto_increment
        primary key,
    user_id bigint unsigned not null comment 'user表 ID',
    amount  int default 0   not null comment '余额'
)
    comment '用户信用卡表' charset = utf8mb4;

create table if not exists gen.languages
(
    id   bigint unsigned auto_increment
        primary key,
    name varchar(128) default '' not null comment '预言'
)
    comment '语言表' charset = utf8mb4;

create table if not exists gen.user_language_relations
(
    id          bigint unsigned auto_increment
        primary key,
    user_id     bigint unsigned not null comment 'user 表 ID',
    language_id bigint unsigned not null comment 'language 表 ID'
)
    comment '用户和语言关系表' charset = utf8mb4;

create table if not exists gen.user_profiles
(
    id      bigint unsigned auto_increment
        primary key,
    user_id bigint unsigned    not null comment 'user表 ID',
    sex     tinyint            not null comment '性别 0:undefined 1 男 2女',
    age     smallint default 0 not null comment '年龄'
)
    comment '用户详情表' charset = utf8mb4;

create table if not exists gen.users
(
    id   bigint unsigned auto_increment
        primary key,
    name varchar(256) default '' not null comment '名字'
)
    comment '用户主表' charset = utf8mb4;


INSERT INTO gen.credit_cards (id, user_id, amount) VALUES (1, 1, 8);
INSERT INTO gen.credit_cards (id, user_id, amount) VALUES (2, 2, 99);
INSERT INTO gen.credit_cards (id, user_id, amount) VALUES (3, 1, 777);

INSERT INTO gen.languages (id, name) VALUES (1, 'en');
INSERT INTO gen.languages (id, name) VALUES (2, 'zh');
INSERT INTO gen.languages (id, name) VALUES (3, 'fr');


INSERT INTO gen.user_language_relations (id, user_id, language_id) VALUES (1, 1, 1);
INSERT INTO gen.user_language_relations (id, user_id, language_id) VALUES (2, 2, 2);
INSERT INTO gen.user_language_relations (id, user_id, language_id) VALUES (3, 1, 2);
INSERT INTO gen.user_language_relations (id, user_id, language_id) VALUES (4, 1, 3);
INSERT INTO gen.user_language_relations (id, user_id, language_id) VALUES (5, 2, 3);
INSERT INTO gen.user_language_relations (id, user_id, language_id) VALUES (6, 3, 1);


INSERT INTO gen.user_profiles (id, user_id, sex, age) VALUES (1, 1, 0, 11);
INSERT INTO gen.user_profiles (id, user_id, sex, age) VALUES (2, 1, 1, 99);
INSERT INTO gen.user_profiles (id, user_id, sex, age) VALUES (3, 2, 1, 66);


INSERT INTO gen.users (id, name) VALUES (1, 'xiaoming');
INSERT INTO gen.users (id, name) VALUES (2, 'lilei');
INSERT INTO gen.users (id, name) VALUES (3, 'wanglei');


-- https://apgquxnnql.feishu.cn/docx/XfRkdSwpgo1ch9xEFJIcV4wpnud