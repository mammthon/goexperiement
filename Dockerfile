FROM golang:1.21
LABEL authors="yanjinbin"
ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct \
    GOOS=linux \
    CGO_ENABLED=0 \
    GOARCH=amd64
WORKDIR /go/src
COPY build/dentistEaseApp /go/src/
COPY app/gorm_preload_association/src/conf/config-prod.yaml /go/src/configs/config.yaml
EXPOSE 8000
CMD ["./dentistEaseApp","run","admin"]