package router

import (
	"net/http"
	"time"

	"goexperiment/app/ep01/gorm_preload_association/src/conf"
	"goexperiment/app/ep01/gorm_preload_association/src/service"
	"goexperiment/app/pkg/src/middlewares/auth"

	"github.com/gin-gonic/gin"
)

var (
	svc    *service.Service
	permit *auth.Permit
)

func Init(cfg *conf.Config) {
	svc = service.New(cfg)
	permit = auth.New(cfg.Jwt)
}

func RegisterRouter(g *gin.RouterGroup) {
	g.GET("/users/:id", UserDetail)
}

// RegisterHealthRouter is register health check gorm_preload_association
func RegisterHealthRouter(r *gin.Engine, env string, commitHash string, buildTime string) {
	r.GET("health", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"status":     "ok",
			"env":        env,
			"commitHash": commitHash,
			"buildTime":  buildTime,
			"timestamp":  time.Now(),
		})
	})
}
