package router

import (
	"github.com/gin-gonic/gin"
	"goexperiment/app/pkg/src/general/response"
	"net/http"
	"strconv"
	"time"
)

func UserDetail(c *gin.Context) {
	idStr := c.Param("id")
	id, _ := strconv.ParseInt(idStr, 10, 64)
	user, _, _ := svc.UserDetail(c.Request.Context(), id)
	c.JSON(http.StatusOK,
		response.Response{
			Code:      0,
			Message:   "success",
			Timestamp: time.Now(),
			Data:      user,
		})
}
