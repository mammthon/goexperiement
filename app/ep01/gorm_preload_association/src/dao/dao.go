package dao

import (
	"strings"

	"goexperiment/app/ep01/gorm_preload_association/src/conf"

	openapi "github.com/alibabacloud-go/darabonba-openapi/v2/client"
	dysmsV3 "github.com/alibabacloud-go/dysmsapi-20170525/v3/client"
	"github.com/alibabacloud-go/tea/tea"
	"github.com/go-resty/resty/v2"
	"github.com/redis/go-redis/v9"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"gopkg.in/gomail.v2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"moul.io/zapgorm2"
)

type Dao struct {
	c          conf.Config
	boilerDB   *gorm.DB
	redis      *redis.Client
	producer   *kafka.Writer
	consumer   *kafka.Reader
	email      *gomail.Dialer
	httpClient *resty.Client
	sms        *dysmsV3.Client
}

func New(cfg *conf.Config) *Dao {
	// 设置gorm logger 为zap
	logger := zapgorm2.New(zap.L())
	logger.SetAsDefault() // optional: configure gorm to use this zapgorm.Logger for callbacks
	datetimePrecision := 2
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       cfg.Mysql.Dsn, // data source name, refer https://github.com/go-sql-driver/mysql#dsn-data-source-name
		DefaultStringSize:         256,           // string 类型字段的默认长度
		DisableDatetimePrecision:  true,
		DefaultDatetimePrecision:  &datetimePrecision, // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,               // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,               // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: true,               // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true, // 单数
		},
		Logger: logger,
	})
	if err != nil {
		panic(err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     cfg.Redis.Address,
		Password: cfg.Redis.Password, // no password set
	})

	brokers := strings.Split(cfg.Kafka.Brokers, ",")
	kafkaConsumer := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  cfg.Kafka.GroupID,
		Topic:    cfg.Kafka.Topic,
		MinBytes: 10e3,
		MaxBytes: 10e6,
	})

	kafkaProducer := &kafka.Writer{
		Addr: kafka.TCP(brokers...),
	}

	email := gomail.NewDialer(cfg.Mail.Host, cfg.Mail.Port, cfg.Mail.From, cfg.Mail.Password)

	httpClient := resty.New()

	sms, err := CreateSMSClient(&cfg.Sms.AccessKeyID, &cfg.Sms.AccessKeySecret)
	if err != nil {
		zap.S().Errorf("CreateSMSClient error: %v", err)
		panic(err)
	}

	d := &Dao{
		boilerDB:   db,
		redis:      rdb,
		producer:   kafkaProducer,
		consumer:   kafkaConsumer,
		email:      email,
		httpClient: httpClient,
		sms:        sms,
	}
	return d
}

// CreateSMSClient is 使用AK&SK初始化账号Client
func CreateSMSClient(accessKeyID *string, accessKeySecret *string) (_result *dysmsV3.Client, _err error) {
	config := &openapi.Config{
		// 必填，您的 AccessKey ID
		AccessKeyId: accessKeyID,
		// 必填，您的 AccessKey Secret
		AccessKeySecret: accessKeySecret,
	}
	// Endpoint 请参考 https://api.aliyun.com/product/Dysmsapi
	config.Endpoint = tea.String("dysmsapi.aliyuncs.com")
	_result, _err = dysmsV3.NewClient(config)
	return _result, _err
}
