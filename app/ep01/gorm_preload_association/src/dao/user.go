package dao

import (
	"context"
	"fmt"
	"goexperiment/app/ep01/gorm_preload_association/src/model"
)

func (d *Dao) GetUserDetail(ctx context.Context, id int64) (user model.User, language model.Language, err error) {
	//information = model.Information{}
	//information.ID = id
	//// preload 大小写敏感 而且不是表名 是类名
	//err = d.boilerDB.WithContext(ctx).Model(&model.Information{}).Preload("Student").First(&information).Error
	////  db.Joins("Student").First(&information)
	//// m := &model.Information{}
	//// d.boilerDB.WithContext(ctx).Model(m).Association(m.Student.TableName()).Find(&information)
	// err := db.Model(&Language{}).Preload("Users").Find(&languages).Error

	// err = d.boilerDB.WithContext(ctx).Model(&model.User{}).Preload("Cards").First(&user, id).Error

	err = d.boilerDB.WithContext(ctx).Model(&model.User{}).Preload("Languages").First(&user, id).Error

	//err = d.boilerDB.WithContext(ctx).Model(&model.Language{}).Preload("Users").First(&user, id).Error

	fmt.Printf("student:%+v \n err: %+v\n", user, err)
	return
}
