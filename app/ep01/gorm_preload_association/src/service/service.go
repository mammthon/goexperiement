package service

import (
	"goexperiment/app/ep01/gorm_preload_association/src/conf"
	"goexperiment/app/ep01/gorm_preload_association/src/dao"
)

type Service struct {
	cfg *conf.Config
	dao *dao.Dao
}

func New(c *conf.Config) *Service {
	return &Service{
		dao: dao.New(c),
		cfg: c,
	}
}
