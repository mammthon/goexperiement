package service

import (
	"context"
	"goexperiment/app/ep01/gorm_preload_association/src/model"
)

func (s *Service) UserDetail(c context.Context, id int64) (user model.User, language model.Language, err error) {
	user, language, err = s.dao.GetUserDetail(c, id)
	return
}
