package req

type FooReq struct {
	Bar string `json:"bar"`
}

type RegisterUserReq struct {
	Username string `json:"username"`
	Phone    string `json:"phone"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type PageQuery struct {
	SortBy   string `json:"sort" form:"sort"`
	PageNo   int    `json:"page_no" form:"page_no"`
	PageSize int    `json:"page_size" form:"page_size"`
}
