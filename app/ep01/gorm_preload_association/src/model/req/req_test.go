package req

import (
	"encoding/json"
	"testing"
)

func TestFoo(t *testing.T) {
	req := RegisterUserReq{
		Username: "test",
		Phone:    "12345678901",
		Email:    "yanjinbin@qq.com",
		Password: "123456",
	}
	bytes, _ := json.Marshal(req)
	t.Log(string(bytes))
}
