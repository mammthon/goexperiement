package conf

import (
	"github.com/gookit/color"
	"github.com/spf13/viper"
)

type Config struct {
	Host struct {
		Address string `toml:"address"`
		Port    int    `toml:"port"`
	} `toml:"host"`
	HTTP struct {
		ReadHeaderTimeout int `toml:"readHeaderTimeout"`
	} `toml:"http"`
	Kafka struct {
		Brokers string `toml:"brokers"`
		GroupID string `toml:"groupID"`
		Topic   string `toml:"topic"`
	} `toml:"kafka"`
	Redis struct {
		Address  string `toml:"address"`
		Password string `toml:"password"`
		DB       string `toml:"db"`
	} `toml:"redis"`
	Mysql struct {
		Dsn string `toml:"dsn"`
	} `toml:"mysql"`
	Jwt struct {
		TTL     int    `toml:"TTL"`
		SignKey string `toml:"SignKey"`
	} `toml:"jwt"`
	LogConf struct {
		EnableStdout           bool `toml:"enableStdout"`
		DataDogLogCustomFields struct {
			Namespace string `toml:"namespace"`
			Env       string `toml:"env"`
		} `toml:"dataDogLogCustomFields"`
		AccessLogConf struct {
			Filename       string `toml:"filename"`
			MaxSize        int    `toml:"maxSize"`
			MaxAge         int    `toml:"maxAge"`
			MaxBackups     int    `toml:"maxBackups"`
			EnableCompress bool   `toml:"enableCompress"`
		} `toml:"accessLogConf"`
		ErrorLogConf struct {
			Filename       string `toml:"filename"`
			MaxSize        int    `toml:"maxSize"`
			MaxAge         int    `toml:"maxAge"`
			MaxBackups     int    `toml:"maxBackups"`
			EnableCompress bool   `toml:"enableCompress"`
		} `toml:"errorLogConf"`
	} `toml:"logConf"`

	Wechat struct {
		AppID        string `toml:"appID"`
		RedirectURI  string `toml:"redirectURI"`
		AppSecret    string `toml:"appSecret"`
		ResponseType string `toml:"responseType"`
		Scope        string `toml:"scope"`
		State        string `toml:"state"`
	} `toml:"wechat"`
	Mail struct {
		Host     string `toml:"host"`
		Port     int    `toml:"port"`
		From     string `toml:"from"`
		Password string `toml:"password"`
	} `toml:"mail"`
	Sms struct {
		AccessKeyID     string `toml:"accessKeyID"`
		AccessKeySecret string `toml:"accessKeySecret"`
	} `toml:"sms"`
}

func InitConfig(env, relativeConfigPath string) *Config {
	var cfg Config
	// 设置配置文件路径和文件名
	configName := "config-local"
	switch env {
	case "local", "":
		env = "local"
		configName = "config-local"
	case "test":
		env = "test"
		configName = "config-test"
	case "prod":
		env = "prod"
		configName = "config-prod"
	}
	color.Warn.Prompt("当前读取环境为 %s, 配置文件: %s, 请注意!", env, configName)
	viper.SetConfigName(configName)
	viper.AddConfigPath(relativeConfigPath)

	if err := viper.ReadInConfig(); err != nil {
		color.Warn.Prompt("读取配置文件错误, %s", err)
		panic(err)
	}
	if err := viper.Unmarshal(&cfg); err != nil {
		color.Warn.Prompt("Error unmarshalling config, %s", err)
		panic(err)
	}

	color.Warn.Tips("读取配置文件成功 %+v", viper.ConfigFileUsed())
	color.Warn.Tips("配置文件内容 %+v", cfg)
	return &cfg
}

type JWTConfig struct {
	SigningKey string `mapstructure:"key" json:"key"`
}
