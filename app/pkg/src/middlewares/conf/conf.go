package conf

type JWT struct {
	TTL     int    `toml:"TTL"`
	SignKey string `toml:"SignKey"`
}
