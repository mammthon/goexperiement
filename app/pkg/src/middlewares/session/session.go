package session

import (
	"context"
)

type (
	accountIDKey struct{}
	clientIPKey  struct{}
	userAgentKey struct{}
	// === 以下为app端新增字段 start ===
	// manufacturerKey   struct{}
	// versionNameKey    struct{}
	// platform          struct{}
	// oaIDKey           struct{}
	// appLanguageKey    struct{}
	// timeZoneKey       struct{}
	// systemLanguageKey struct{}
	// deviceIDKey       struct{}
	// gaidKey           struct{}
	// platformKey       struct{}
	// deviceTypeKey     struct{}
	// === 以下为app端新增字段 end ===
)

func SetUserID(ctx context.Context, userID int64) context.Context {
	return context.WithValue(ctx, accountIDKey{}, userID)
}

func GetUserID(ctx context.Context) int64 {
	userID, ok := ctx.Value(accountIDKey{}).(int64)
	if ok {
		return userID
	}
	return userID
}

func SetClientIP(ctx context.Context, ip string) context.Context {
	return context.WithValue(ctx, clientIPKey{}, ip)
}

func GetClientIP(ctx context.Context) string {
	ip, ok := ctx.Value(clientIPKey{}).(string)
	if ok {
		return ip
	}
	return ""
}

func SetUserAgent(ctx context.Context, userAgent string) context.Context {
	return context.WithValue(ctx, userAgentKey{}, userAgent)
}

func GetUserAgent(ctx context.Context) string {
	if userAgent, ok := ctx.Value(userAgentKey{}).(string); ok {
		return userAgent
	}
	return ""
}

/*
func SetManufacturer(ctx context.Context, manufacturer string) context.Context {
	return context.WithValue(ctx, manufacturerKey{}, manufacturer)
}

func SetOs(ctx context.Context, os string) context.Context {
	return context.WithValue(ctx, platform{}, cast.ToInt32(os))
}

func SetVersionName(ctx context.Context, versionName string) context.Context {
	return context.WithValue(ctx, versionNameKey{}, versionName)
}
func SetOaID(ctx context.Context, oa string) context.Context {
	return context.WithValue(ctx, oaIDKey{}, oa)
}

func GetOaID(ctx context.Context) string {
	if oaID, ok := ctx.Value(oaIDKey{}).(string); ok {
		return oaID
	}
	return ""
}

func GetManufacturer(ctx context.Context) string {
	if device, ok := ctx.Value(manufacturerKey{}).(string); ok {
		return device
	}
	return ""
}

func GetVersionName(ctx context.Context) string {
	if device, ok := ctx.Value(versionNameKey{}).(string); ok {
		return device
	}
	return ""
}

func GetOs(ctx context.Context) int {
	if device, ok := ctx.Value(platform{}).(int); ok {
		return device
	}
	return 0
}

func SetDeviceType(ctx context.Context, deviceType string) context.Context {
	return context.WithValue(ctx, deviceTypeKey{}, deviceType)
}

func GetDeviceType(ctx context.Context) string {
	if device, ok := ctx.Value(deviceTypeKey{}).(string); ok {
		return device
	}
	return ""
}

func SetAppLanguage(ctx context.Context, appLanguage string) context.Context {
	return context.WithValue(ctx, appLanguageKey{}, appLanguage)
}

func GetAppLanguage(ctx context.Context) string {
	if appLang, ok := ctx.Value(appLanguageKey{}).(string); ok {
		return appLang
	}
	return ""
}

func SetSystemLanguage(ctx context.Context, systemLanguage string) context.Context {
	return context.WithValue(ctx, systemLanguageKey{}, systemLanguage)
}

func GetSystemLanguage(ctx context.Context) string {
	if systemLanguage, ok := ctx.Value(systemLanguageKey{}).(string); ok {
		return systemLanguage
	}
	return ""
}

func SetTimeZone(ctx context.Context, timeZone string) context.Context {
	return context.WithValue(ctx, timeZoneKey{}, timeZone)
}

func GetTimeZone(ctx context.Context) string {
	if tz, ok := ctx.Value(timeZoneKey{}).(string); ok {
		return tz
	}
	return ""
}

func SetDeviceID(ctx context.Context, deviceID string) context.Context {
	return context.WithValue(ctx, deviceIDKey{}, deviceID)
}

func GetDeviceID(ctx context.Context) string {
	if deviceID, ok := ctx.Value(deviceIDKey{}).(string); ok {
		return deviceID
	}
	return ""
}

func SetGAID(ctx context.Context, gaid string) context.Context {
	return context.WithValue(ctx, gaidKey{}, gaid)
}

func GetGAID(ctx context.Context) string {
	if gaid, ok := ctx.Value(gaidKey{}).(string); ok {
		return gaid
	}
	return ""
}

func SetPlatform(ctx context.Context, platform string) context.Context {
	return context.WithValue(ctx, platformKey{}, platform)
}

func GetPlatform(ctx context.Context) string {
	if platform, ok := ctx.Value(platformKey{}).(string); ok {
		return platform
	}
	return ""
}
*/
