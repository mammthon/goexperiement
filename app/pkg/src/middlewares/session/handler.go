package session

import (
	"github.com/gin-gonic/gin"
)

func Session() gin.HandlerFunc {
	return func(c *gin.Context) {
		ua := c.GetHeader("User-Agent")
		ctx := c.Request.Context()
		ctx = SetClientIP(ctx, c.ClientIP())
		// HTTP RFC 7231, section 5.5.3
		ctx = SetUserAgent(ctx, ua)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}
