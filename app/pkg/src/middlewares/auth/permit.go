package auth

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"goexperiment/app/pkg/src/general/response"
	"goexperiment/app/pkg/src/middlewares/conf"
	"goexperiment/app/pkg/src/middlewares/session"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
)

const (
	BlackListPrefix = "app#blacklist#"
)

type CustomClaims struct {
	UserID       int64  `json:"UserID"`
	Exp          int64  `json:"exp"`
	Role         string `json:"role"`
	RegisterTime string `json:"register_time"`
	jwt.RegisteredClaims
}

var (
	ErrTokenInvalid   = errors.New("invalid token")
	ErrTokenExpired   = errors.New("token expired")
	ErrTokenMalformed = errors.New("token malformed")
	ErrBlackListUser  = errors.New("black list user")
)

type Permit struct {
	JWT         conf.JWT
	redisClient *redis.Client
}

// New  create a new jwt permit instance
func New(jwt conf.JWT) *Permit {
	p := &Permit{
		JWT: jwt,
	}
	return p
}

// Verify only export Verify function because of less configure
type Verify interface {
	Verify() gin.HandlerFunc
}

func (p *Permit) Verify() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")
		if token == "" {
			c.JSON(http.StatusUnauthorized, response.Response{
				Code:      http.StatusUnauthorized,
				Message:   "empty token, please login",
				Timestamp: time.Now(),
			})
			c.Abort()
			return
		}
		claims, err := p.verifyToken(c, token)
		if err != nil {
			c.JSON(http.StatusUnauthorized, response.Response{
				Code:      http.StatusUnauthorized,
				Message:   err.Error(),
				Timestamp: time.Now(),
			})
			c.Abort()
			return
		}
		ctx := c.Request.Context()
		ctx = session.SetUserID(ctx, claims.UserID)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

// GenerateToken generate a jwt token
// 保证生成的token 都是在有效期内
//func (p *Permit) GenerateToken(user *model.User) (signedToken string, err error) {
//	return "", nil
//	/*// 创建 CustomClaims 对象
//	expireAt := time.Now().Add(time.Duration(p.JWT.TTL) * time.Hour)
//	if user.ExpireTime != nil && user.ExpireTime.After(time.Now()) {
//		// nolint: goerr113
//		return "", errors.New("user expired")
//	}
//	if user.ExpireTime != nil && expireAt.After(*user.ExpireTime) {
//		expireAt = *user.ExpireTime
//	}
//	claims := CustomClaims{
//		UserID: user.ID,
//		// "admin", "doctor", "nurse", "receptionist", "finance", "warehouse"
//		// Role:         user.Role,
//		Exp:          expireAt.Unix(),
//		RegisterTime: time.Now().Format(time.RFC3339),
//	}
//
//	// 创建 Token 对象
//	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
//
//	// 使用密钥(secret)进行签名
//	signedToken, err = token.SignedString([]byte(p.JWT.SignKey))
//	if err != nil {
//		return "", err
//	}
//
//	return signedToken, nil*/
//}

func (p *Permit) verifyToken(c context.Context, tokenString string) (*CustomClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(p.JWT.SignKey), nil
	})
	if err != nil {
		zap.S().Errorf("parse with claims failed. error: %s", err.Error())
		return nil, ErrTokenMalformed
	}

	// 验证 Token 中的声明(claims)
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		// 我们可以通过 claims 对象访问声明中的字段
		userID := claims.UserID
		if blackUser := p.BlackList(c, userID); blackUser {
			return nil, ErrBlackListUser
		}
		role := claims.Role
		exp := claims.Exp
		registerTime := claims.RegisterTime
		zap.S().Infof("userID: %d, role: %s, exp: %d, registerTime: %s", userID, role, exp, registerTime)

		// 验证过期时间
		if time.Now().Unix() >= exp {
			return nil, ErrTokenExpired
		}

		// 其他验证逻辑...
		return claims, nil
	}

	return nil, ErrTokenInvalid
}

func (p *Permit) BlackList(ctx context.Context, userID int64) (exist bool) {
	key := fmt.Sprintf("%s%d", BlackListPrefix, userID)
	_, err := p.redisClient.Get(ctx, key).Result()
	if err != nil {
		zap.S().Errorf("redis get failed. error: %s", err.Error())
		return false
	}
	return true
}

func (p *Permit) Close() {
	p.redisClient.Close()
}
