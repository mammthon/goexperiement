package xerror

import "github.com/rotisserie/eris"

var (
	ErrInternalServer      = New("error internal server").WithCode(BusinessCode(500)).Build()
	ErrInvalidInput        = New("error invalid input").WithCode(BusinessCode(400)).Build()
	ErrDeviceIDNotFound    = New("error device id not found").WithCode(BusinessCode(1000)).Build()
	ErrCoinsBeansNotEnough = New("error coins and beans not enough").WithCode(BusinessCode(1001)).Build()
	ErrExceedNumUnlockAd   = New("error exceed num unlock ad").WithCode(BusinessCode(1002)).Build()
	ErrSeasonNotFound      = New("error season not found").WithCode(BusinessCode(1003)).Build()
)

type BusinessCode int

type XError struct {
	eris.ErrRoot
	Code BusinessCode
}

type XErrBuilder struct {
	msg  string
	code BusinessCode
}

func New(msg string) *XErrBuilder {
	return &XErrBuilder{
		msg: msg,
		// default
		code: -1,
	}
}

func (xb *XErrBuilder) WithCode(code BusinessCode) *XErrBuilder {
	xb.code = code
	return xb
}

func (xb *XErrBuilder) Build() *XError {
	return &XError{
		ErrRoot: eris.ErrRoot{Msg: xb.msg},
		Code:    xb.code,
	}
}

func (x *XError) Error() string {
	return x.ErrRoot.Msg
}
