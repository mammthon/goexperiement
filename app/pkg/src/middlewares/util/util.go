package util

import "strconv"

func CalLimitOffset(pageNo, pageSize int) (limit int, offset int) {
	if pageNo <= 0 {
		pageNo = 1
	}
	if pageSize <= 0 {
		pageSize = 10
	}
	return pageSize, (pageNo - 1) * pageSize
}

func FormArrayString(arr []int64) string {
	var res string
	for i, v := range arr {
		if i != 0 {
			res += ","
		}
		res += strconv.FormatInt(v, 10)
	}
	return res
}
