package middlewares

import (
	"bytes"
	"fmt"
	"io"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// CustomResponseWriter 封装 gin ResponseWriter 用于获取回包内容。
type CustomResponseWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w CustomResponseWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

// LoggerCurl 日志中间件
func LoggerCurl() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 记录请求时间
		start := time.Now()

		// 使用自定义 ResponseWriter
		crw := &CustomResponseWriter{
			body:           bytes.NewBufferString(""),
			ResponseWriter: c.Writer,
		}
		c.Writer = crw

		// 打印请求信息
		LogCurl(c)

		// 执行请求处理程序和其他中间件函数
		c.Next()

		// 记录回包内容和处理时间
		end := time.Now()
		latency := end.Sub(start)
		respBody := crw.body.String()
		zap.S().Debugf("Response: %v\nLatency: (%v)", respBody, latency)
	}
}

func LogCurl(c *gin.Context) {
	curl := fmt.Sprintf("curl -X %s '%s'", c.Request.Method, c.Request.URL.String())

	// 打印请求头部
	for key, values := range c.Request.Header {
		for _, value := range values {
			curl += fmt.Sprintf(" -H '%s: %s'", key, value)
		}
	}

	// 打印请求主体
	body, _ := io.ReadAll(c.Request.Body)
	if len(body) > 0 {
		curl += fmt.Sprintf(" --data '%s'", string(body))
	}
	zap.S().Debugf("\n%s", curl)
	// 重新设置请求主体
	c.Request.Body = io.NopCloser(bytes.NewBuffer(body))
}
