package log

type RotateOpt struct {
	MaxSize    int  `toml:"maxSize"`
	MaxAge     int  `toml:"maxAge"`
	MaxBackups int  `toml:"maxBackups"`
	Compress   bool `toml:"compress"`
}

type FileConf struct {
	RotateOpt
	FileName string `toml:"fileName"`
}

type DataDogLogCustomFields struct {
	Namespace string `toml:"namespace"`
	Env       string `toml:"env"`
}

type Config struct {
	AccessLogConf          FileConf `toml:"accessLogConf"`
	ErrorLogConf           FileConf `toml:"errorLogConf"`
	DataDogLogCustomFields `toml:"dataDogLogCustomFields"`
	EnableStdout           bool `toml:"enableStdout"`
}
