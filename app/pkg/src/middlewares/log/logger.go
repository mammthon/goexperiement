package log

import (
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime/debug"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

// InitLogger is init logger, logs output to file
func InitLogger(cfg *Config) {
	nameSpaceField := zapcore.Field{
		Key:    "namespace",
		Type:   zapcore.StringType,
		String: cfg.DataDogLogCustomFields.Namespace,
	}
	envField := zapcore.Field{
		Key:    "env",
		Type:   zapcore.StringType,
		String: cfg.DataDogLogCustomFields.Env,
	}

	tops := []TeeOption{
		{
			Filename: cfg.AccessLogConf.FileName,
			Ropt: RotateOptions{
				MaxSize:    cfg.AccessLogConf.MaxSize,
				MaxAge:     cfg.AccessLogConf.MaxAge,
				MaxBackups: cfg.AccessLogConf.MaxBackups,
				Compress:   cfg.AccessLogConf.Compress,
			},
			Lef: func(lvl Level) bool {
				return lvl <= InfoLevel
			},
		},
		{
			Filename: cfg.ErrorLogConf.FileName,
			Ropt: RotateOptions{
				MaxSize:    cfg.ErrorLogConf.MaxSize,
				MaxAge:     cfg.ErrorLogConf.MaxAge,
				MaxBackups: cfg.ErrorLogConf.MaxBackups,
				Compress:   cfg.ErrorLogConf.Compress,
			},
			Lef: func(lvl Level) bool {
				return lvl > InfoLevel
			},
		},
	}

	cores := make([]zapcore.Core, 0)
	prodCfg := zap.NewProductionConfig()
	prodCfg.EncoderConfig.EncodeTime = zapcore.RFC3339NanoTimeEncoder
	prodCfg.EncoderConfig.TimeKey = "@timestamp"
	prodCfg.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	prodCfg.EncoderConfig.EncodeCaller = zapcore.ShortCallerEncoder
	prodCfg.EncoderConfig.CallerKey = "file"

	for _, top := range tops {
		top := top
		lv := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return top.Lef(lvl)
		})
		w := zapcore.AddSync(&lumberjack.Logger{
			Filename:   top.Filename,
			MaxSize:    top.Ropt.MaxSize,
			MaxBackups: top.Ropt.MaxBackups,
			MaxAge:     top.Ropt.MaxAge,
			Compress:   top.Ropt.Compress,
		})
		writers := []zapcore.WriteSyncer{zapcore.AddSync(w)}
		encoder := zapcore.NewConsoleEncoder(prodCfg.EncoderConfig)
		if cfg.EnableStdout {
			writers = append(writers, zapcore.AddSync(os.Stdout))
			encoder = zapcore.NewConsoleEncoder(prodCfg.EncoderConfig)
		}
		core := zapcore.NewCore(
			encoder,
			zapcore.NewMultiWriteSyncer(writers...),
			lv,
		)
		cores = append(cores, core)
	}
	tee := zapcore.NewTee(cores...)
	options := []Option{
		zap.WithCaller(true),
		zap.AddStacktrace(zapcore.WarnLevel),
	}
	if !cfg.EnableStdout {
		options = append(options, zap.Fields(nameSpaceField, envField))
	}
	ll := zap.New(tee, options...)
	zap.ReplaceGlobals(ll)
}

// GinLogger 接收gin框架默认的日志
func GinLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		c.Next()

		cost := time.Since(start)
		zap.L().Info(path,
			//	zap.String("req"),
			zap.Int("status", c.Writer.Status()),
			zap.String("method", c.Request.Method),
			zap.String("path", path),
			zap.String("query", query),

			zap.String("ip", c.ClientIP()),
			zap.String("user-agent", c.Request.UserAgent()),
			zap.String("errors", c.Errors.ByType(gin.ErrorTypePrivate).String()),
			zap.Duration("cost", cost),
		)
	}
}

// GinRecovery recover掉项目可能出现的panic，并使用zap记录相关日志
func GinRecovery(stack bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic stack trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					//nolint: errorlint
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") ||
							strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				httpRequest, _ := httputil.DumpRequest(c.Request, false)
				if brokenPipe {
					zap.L().Error(c.Request.URL.Path,
						zap.Any("error", err),
						zap.String("req", string(httpRequest)),
					)
					// If the connection is dead, we can't write a status to it.
					c.Error(err.(error)) // nolint: errcheck
					c.Abort()
					return
				}

				if stack {
					zap.L().Error("[Recovery from panic]",
						zap.Any("error", err),
						zap.String("req", string(httpRequest)),
						zap.String("stack", string(debug.Stack())),
					)
				} else {
					zap.L().Error("[Recovery from panic]",
						zap.Any("error", err),
						zap.String("req", string(httpRequest)),
					)
				}
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}()
		c.Next()
	}
}

type Option = zap.Option

var (
	WithCaller    = zap.WithCaller
	AddStacktrace = zap.AddStacktrace
)

type RotateOptions struct {
	MaxSize    int
	MaxAge     int
	MaxBackups int
	Compress   bool
}

type LevelEnablerFunc func(lvl Level) bool

type TeeOption struct {
	Filename string
	Ropt     RotateOptions
	Lef      LevelEnablerFunc
}

const (
	InfoLevel   Level = zap.InfoLevel   // 0, default level
	WarnLevel   Level = zap.WarnLevel   // 1
	ErrorLevel  Level = zap.ErrorLevel  // 2
	DPanicLevel Level = zap.DPanicLevel // 3, used in development logs
	// PanicLevel logs a message, then panics
	PanicLevel Level = zap.PanicLevel // 4
	// FatalLevel logs a message, then calls os.Exit(1).
	FatalLevel Level = zap.FatalLevel // 5
	DebugLevel Level = zap.DebugLevel // -1
)

type Level = zapcore.Level
