package response

import "time"

type Response struct {
	Code      int         `json:"code" example:"0"`
	Data      interface{} `json:"data"`
	Message   string      `json:"msg" example:"success"`
	Timestamp time.Time   `json:"timestamp" example:"2005-08-15T15:52:01+08:00"`
}
