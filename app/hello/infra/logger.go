package infra

import (
	"goexperiment/app/hello/conf"
	"goexperiment/app/pkg/src/middlewares/log"
)

func InitLogger(cfg *conf.Config) {
	config := log.Config{
		DataDogLogCustomFields: cfg.LogConf.DataDogLogCustomFields,
		AccessLogConf: log.FileConf{
			RotateOpt: log.RotateOpt{
				MaxSize:    cfg.LogConf.AccessLogConf.MaxSize,
				MaxAge:     cfg.LogConf.AccessLogConf.MaxAge,
				MaxBackups: cfg.LogConf.AccessLogConf.MaxBackups,
				Compress:   cfg.LogConf.AccessLogConf.EnableCompress,
			},
			FileName: cfg.LogConf.AccessLogConf.Filename,
		},
		ErrorLogConf: log.FileConf{
			RotateOpt: log.RotateOpt{
				MaxSize:    cfg.LogConf.ErrorLogConf.MaxSize,
				MaxAge:     cfg.LogConf.ErrorLogConf.MaxAge,
				MaxBackups: cfg.LogConf.ErrorLogConf.MaxBackups,
				Compress:   cfg.LogConf.ErrorLogConf.EnableCompress,
			},
			FileName: cfg.LogConf.ErrorLogConf.Filename,
		},
		EnableStdout: cfg.LogConf.EnableStdout,
	}
	log.InitLogger(&config)
}
