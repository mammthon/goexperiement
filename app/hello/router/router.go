package router

import (
	"goexperiment/app/hello/conf"
	"goexperiment/app/hello/service"
	"goexperiment/app/pkg/src/middlewares/auth"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

var (
	svc    *service.Service
	permit *auth.Permit
)

func Init(cfg *conf.Config) {
	svc = service.New(cfg)
	permit = auth.New(cfg.Jwt)
}

func RegisterRouter(g *gin.RouterGroup) {
	g.GET("/users/:id", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})
}

// RegisterHealthRouter is register health check gorm_preload_association
func RegisterHealthRouter(r *gin.Engine, env string, commitHash string, buildTime string) {
	r.GET("health", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"status":     "ok",
			"env":        env,
			"commitHash": commitHash,
			"buildTime":  buildTime,
			"timestamp":  time.Now(),
		})
	})
}
