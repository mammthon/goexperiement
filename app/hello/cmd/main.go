package main

import (
	"context"
	"errors"
	"fmt"
	"goexperiment/app/hello/conf"
	"goexperiment/app/hello/infra"
	"goexperiment/app/hello/router"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	pkgCurl "goexperiment/app/pkg/src/middlewares/curl"
	pkgLog "goexperiment/app/pkg/src/middlewares/log"

	"github.com/gin-gonic/gin"
	"github.com/gookit/color"
)

const (
	BaseURL          = "/goexperiment/app/v1"
	RelativeConfPath = "src/conf"
	// RelativeConfPath = "app/gorm_preload_association/src/conf"
)

var (
	Env        string
	Version    string
	CommitHash string
	BuildTime  string
	NameSpace  string
)

// @title           goexperiment  APP API docs
// @version         1.0
// @contact.email  yanjinbin@qq.com
// @host      127.0.0.1:9999
// @BasePath  /goexperiment/app/v1
func main() {
	cfg := conf.InitConfig(Env, RelativeConfPath)
	infra.InitLogger(cfg)
	router.Init(cfg)
	r := gin.New()
	r.Use(pkgLog.GinLogger(), pkgCurl.LoggerCurl() /*pkgLog.GinRecovery(true)*/)
	mainRouter := r.Group(BaseURL)
	router.RegisterRouter(mainRouter)
	router.RegisterHealthRouter(r, Env, CommitHash, BuildTime)
	serverPort := cfg.Host.Port
	color.Warn.Prompt("\nPort:%v\t Version:%v\t CommitHash:%v\t BuildTime:%v\t NameSpace:%v\t\n", serverPort, Version, CommitHash, BuildTime, NameSpace)

	srv := &http.Server{
		Addr:              fmt.Sprintf(":%d", serverPort),
		Handler:           r,
		ReadHeaderTimeout: time.Duration(cfg.HTTP.ReadHeaderTimeout) * time.Second,
	}
	go func() {
		// 开启goroutine启动服务
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("[Server] [Fatal] Server Start Fatal: %v", err)
		}
		log.Print("[Server] [Info] Server Start")
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Print("[Server] [Info]  Shutting Down Server...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("[Server] [Fatal]  Server Forced To Shutdown: %v", err)
	}
	log.Print("[SERVER] [Info] Exiting")
	defer log.Print("[SERVER] [Info] Closed")
}
