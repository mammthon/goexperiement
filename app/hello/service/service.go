package service

import (
	"goexperiment/app/hello/conf"
	"goexperiment/app/hello/dao"
)

type Service struct {
	cfg *conf.Config
	dao *dao.Dao
}

func New(c *conf.Config) *Service {
	return &Service{
		dao: dao.New(c),
		cfg: c,
	}
}
