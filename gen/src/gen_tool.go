package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gen"
	"gorm.io/gen/field"
	"gorm.io/gorm"
)

const (
	AdminDSN        = "root:123456@tcp(127.0.0.1:3306)/dentist_ease_admin?charset=utf8mb4&parseTime=True&loc=Local&collation=utf8mb4_unicode_ci"
	AppDSN          = "root:123456@tcp(127.0.0.1:3306)/dentist_ease_app?charset=utf8mb4&parseTime=True&loc=Local&collation=utf8mb4_unicode_ci"
	goexperimentDSN = "root:123456@tcp(127.0.0.1:3306)/goexperiment?charset=utf8mb4&parseTime=True&loc=Local&collation=utf8mb4_unicode_ci"
	GenDSN          = "root:123456@tcp(127.0.0.1:3306)/gen?charset=utf8mb4&parseTime=True&loc=Local&collation=utf8mb4_unicode_ci"
)

func main() {
	// GenModel(AdminDSN, "./dao/admin/query")
	// GenModel(AppDSN, "./dao/app/query")
	// GenModel(goexperimentDSN, "./dao/goexperiment/query")
	GenModel(GenDSN, "./dao/hello/query")
}

func GenModel(dsn string, rootQueryGenDir string) {
	// 连接数据库
	db, err := gorm.Open(mysql.Open(dsn))
	if err != nil {
		panic(fmt.Errorf("cannot establish db connection: %w", err))
	}

	// 生成实例
	g := gen.NewGenerator(gen.Config{
		// 相对执行`go run`时的路径, 会自动创建目录

		OutPath: rootQueryGenDir,

		// WithDefaultQuery 生成默认查询结构体(作为全局变量使用), 即`Q`结构体和其字段(各表模型)
		// WithoutContext 生成没有context调用限制的代码供查询
		// WithQueryInterface 生成interface形式的查询代码(可导出), 如`Where()`方法返回的就是一个可导出的接口类型
		Mode: gen.WithDefaultQuery | gen.WithQueryInterface,

		// 表字段可为 null 值时, 对应结体字段使用指针类型
		FieldNullable: true, // generate pointer when field is nullable

		// 表字段默认值与模型结构体字段零值不一致的字段,
		// 在插入数据时需要赋值该字段值为零值的, 结构体字段须是指针类型才能成功,
		// 即`FieldCoverable:true`配置下生成的结构体字段.
		// 因为在插入时遇到字段为零值的会被GORM赋予默认值.
		// 如字段`age`表默认值为10, 即使你显式设置为0最后也会被GORM设为10提交.
		// 如果该字段没有上面提到的插入时赋零值的特殊需要, 则字段为非指针类型使用起来会比较方便.
		FieldCoverable: false, // generate pointer when field has default value, to fix problem zero value cannot be assign: https://gorm.io/docs/create.html#Default-Values

		// 模型结构体字段的数字类型的符号表示是否与表字段的一致, `false`指示都用有符号类型
		FieldSignable: false, // detect integer field's unsigned type, adjust generated data type
		// 生成 gorm 标签的字段索引属性
		FieldWithIndexTag: true, // generate with gorm index tag
		// 生成 gorm 标签的字段类型属性
		FieldWithTypeTag: true, // generate with gorm column type tag

	})

	// 为数据库设置外键
	// create database foreign key for user & credit_cards
	// db.Migrator().CreateConstraint(&User{}, "CreditCards")
	// db.Migrator().CreateConstraint(&User{}, "fk_users_credit_cards")
	// 设置目标 db
	g.UseDB(db)

	// 自定义字段的数据类型
	// 统一数字类型为int64,兼容protobuf
	// map[string]func(columnType gorm.ColumnType)
	dataMap := map[string]func(columnType gorm.ColumnType) (dataType string){
		"tinyint":   func(detailType gorm.ColumnType) (dataType string) { return "int" },
		"smallint":  func(detailType gorm.ColumnType) (dataType string) { return "int" },
		"mediumint": func(detailType gorm.ColumnType) (dataType string) { return "int" },
		"int":       func(detailType gorm.ColumnType) (dataType string) { return "int" },
		"bigint":    func(detailType gorm.ColumnType) (dataType string) { return "int64" },
	}
	// 要先于`ApplyBasic`执行
	g.WithDataTypeMap(dataMap)

	// 自定义模型结体字段的标签
	// 将特定字段名的 json 标签加上`string`属性,即 MarshalJSON 时该字段由数字类型转成字符串类型
	/*jsonField := gen.FieldJSONTagWithNS(func(columnName string) (tagContent string) {
		toStringField := `balance, `
		if strings.Contains(toStringField, columnName) {
			return columnName + ",string"
		}
		return columnName
	})*/

	// 模型自定义选项组
	// 将非默认字段名的字段定义为自动时间戳和软删除字段;
	// 自动时间戳默认字段名为:`updated_at`、`created_at, 表字段数据类型为: INT 或 DATETIME
	// 软删除默认字段名为:`deleted_at`, 表字段数据类型为: DATETIME

	//  gormTag func(tag field.GormTag) field.GormTag
	/*autoUpdateTimeField := gen.FieldGORMTag("update_time", func(tag field.GormTag) field.GormTag {
		return field.GormTag{
			"column":         "update_time",
			"type":           "int unsigned",
			"autoUpdateTime": "",
		}
	})*/

	/*autoCreateTimeField := gen.FieldGORMTag("create_time", func(tag field.GormTag) field.GormTag {
		return field.GormTag{
			"column":         "create_time",
			"type":           "int unsigned",
			"autoCreateTime": "",
		}
	})*/

	// 软删除字段
	softDeleteField := gen.FieldType("deleted_at", "gorm.DeletedAt")

	// 模型自定义选项组
	fieldOpts := []gen.ModelOpt{ /*autoCreateTimeField, autoUpdateTimeField,*/ softDeleteField}

	card := g.GenerateModel("credit_cards")
	// 创建全部模型文件, 并覆盖前面创建的同名模型
	allModel := g.GenerateAllTable(fieldOpts...)
	// 创建表时添加后缀
	// db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&User{}, &Language{})

	g.GenerateModel("users", append(fieldOpts, gen.FieldRelate(
		field.HasMany, "Cards", card, &field.RelateConfig{GORMTag: map[string][]string{
			"foreignKey": []string{"userID"},
		}}))...)

	g.ApplyBasic(allModel...)

	g.Execute()
}

//
//// User 拥有并属于多种 language，`user_languages` 是连接表
//type User struct {
//	gorm.Model
//	Languages []Language `gorm:"many2many:user_languages;"`
//}
//
//type Language struct {
//	gorm.Model
//	Name string
//}

//
//// User mapped from table <user>
//type User struct {
//	ID        int64          `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement:true" json:"id"`                    // ID
//	Name      string         `gorm:"column:name;type:varchar(20);not null" json:"name"`                                      // 用户名
//	Age       int64          `gorm:"column:age;type:tinyint unsigned;not null" json:"age"`                                   // 年龄
//	Balance   float64        `gorm:"column:balance;type:decimal(11,2) unsigned;not null;default:0.00" json:"balance,string"` // 余额
//	UpdatedAt time.Time      `gorm:"column:updated_at;type:datetime;not null" json:"updated_at"`                             // 更新时间
//	CreatedAt time.Time      `gorm:"column:created_at;type:datetime;not null" json:"created_at"`                             // 创建时间
//	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at;type:datetime" json:"deleted_at"`                                      // 删除时间
//	Address   []Address      `gorm:"foreignKey:UID" json:"address"`
//
//	//ID        int            `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement:true" json:"id"`
//	//Name      string         `gorm:"column:name;type:varchar(20);not null" json:"name"`
//	//Age       int            `gorm:"column:age;type:tinyint unsigned;not null" json:"age"`
//	//Balance   float64        `gorm:"column:balance;type:decimal(11,2) unsigned;not null;default:0.00" json:"balance"`
//	//UpdatedAt time.Time      `gorm:"column:updated_at;type:datetime;not null" json:"updated_at"`
//	//CreatedAt time.Time      `gorm:"column:created_at;type:datetime;not null" json:"created_at"`
//	//DeletedAt gorm.DeletedAt `gorm:"column:deleted_at;type:datetime" json:"deleted_at"`
//}
//
//// Address mapped from table <address>
//type Address struct {
//	ID         int64          `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement:true" json:"id"`
//	UID        int64          `gorm:"column:uid;type:int unsigned;not null" json:"uid"`
//	Province   string         `gorm:"column:province;type:varchar(20);not null" json:"province"`
//	City       string         `gorm:"column:city;type:varchar(20);not null" json:"city"`
//	UpdateTime int64          `gorm:"column:update_time;type:int unsigned;autoUpdateTime" json:"update_time"`
//	CreateTime int64          `gorm:"column:create_time;type:int unsigned;autoCreateTime" json:"create_time"`
//	DeleteTime gorm.DeletedAt `gorm:"column:delete_time;type:int unsigned;not null" json:"delete_time"`
//	User       User           `gorm:"foreignKey:UID" json:"user"`
//
//	//ID         int    `gorm:"column:id;type:int unsigned;primaryKey;autoIncrement:true" json:"id"`
//	//UID        int    `gorm:"column:uid;type:int unsigned;not null;index:fk_users_address,priority:1" json:"uid"`
//	//Province   string `gorm:"column:province;type:varchar(20);not null" json:"province"`
//	//City       string `gorm:"column:city;type:varchar(20);not null" json:"city"`
//	//UpdateTime *int   `gorm:"column:update_time;type:int unsigned" json:"update_time"`
//	//CreateTime *int   `gorm:"column:create_time;type:int unsigned" json:"create_time"`
//	//DeleteTime int    `gorm:"column:delete_time;type:int unsigned;not null" json:"delete_time"`
//}

//
//type User struct {
//	gorm.Model
//	CreditCards []CreditCard
//}
//
//type CreditCard struct {
//	gorm.Model
//	Number string
//	UserID uint
//}
//type Student struct {
//	ID   int64 `gorm:"primary_key"`
//	Name string
//}
//type Information struct {
//	ID        int64 `gorm:"primary_key"`
//	StudentID int64
//	Sex       uint8
//	Age       uint8
//	HT        string  `gorm:"column:hometown"`
//	Student   Student `gorm:"foreignKey:student_id"`
//}
