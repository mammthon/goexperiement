#!/bin/bash

localIP=$(ifconfig | grep 'inet ' | grep -v '127.0.0.1' | awk '{print $2}' | head -n 1)
webhook_url="https://open.feishu.cn/open-apis/bot/v2/hook/e4333dcd-3f6a-41b3-aff8-c957d4150c7a"
username=$(git config user.name)
email=$(git config user.email)

curl -X POST -H "Content-Type: application/json" -d "{\"msg_type\":\"text\",\"content\":{\"text\":\"swagger更新啦 $email $username 喊你快来看看 $localIP:7777\"}}" "$webhook_url"
